import { sync as commandExistsSync } from 'command-exists';

/**
 * Check if pnpm is available.
 *
 * @exports
 * @returns {boolean}
 */
function caniusePNPM(): boolean {
	return commandExistsSync('pnpm');
}

export = caniusePNPM;
