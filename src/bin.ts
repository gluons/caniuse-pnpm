#!/usr/bin/env node

import chalk from 'chalk';
import { cross, tick } from 'figures';
import { EOL } from 'os';
import yargs from 'yargs';

import caniusePNPM from './index';

const { cyan, green, red } = chalk;
const pnpm = cyan('pnpm');

yargs
	.scriptName(cyan('caniuse-pnpm'))
	.help()
	.version()
	.alias('help', 'h')
	.alias('version', 'v')
	.usage('$0', 'Check if pnpm is available.').argv;

if (caniusePNPM()) {
	process.stdout.write(
		green(`${tick} ${pnpm} is available. You can use ${pnpm}.${EOL}`)
	);
} else {
	process.stdout.write(
		red(`${cross} ${pnpm} is not available! You cannot use ${pnpm}.${EOL}`)
	);
}
