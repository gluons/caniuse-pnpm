# caniuse-pnpm
[![NPM](https://img.shields.io/npm/l/caniuse-pnpm.svg?style=flat-square)](https://gitlab.com/gluons/caniuse-pnpm/blob/master/LICENSE)
[![npm](https://img.shields.io/npm/v/caniuse-pnpm.svg?style=flat-square)](https://www.npmjs.com/package/caniuse-pnpm)
[![npm](https://img.shields.io/npm/dt/caniuse-pnpm.svg?style=flat-square)](https://www.npmjs.com/package/caniuse-pnpm)
![npm type definitions](https://img.shields.io/npm/types/caniuse-pnpm.svg?style=flat-square)
[![pipeline status](https://gitlab.com/gluons/caniuse-pnpm/badges/master/pipeline.svg?style=flat-square)](https://gitlab.com/gluons/caniuse-pnpm/pipelines)

Check if [pnpm](https://pnpm.js.org/) is available.

## Installation

```bash
npm install caniuse-pnpm
# or
yarn add caniuse-pnpm
# or
pnpm install caniuse-pnpm
```

## CLI

You can also install as global package to use CLI.

```bash
npm install -g caniuse-pnpm
# or
yarn global add caniuse-pnpm
```

```
caniuse-pnpm

Check if pnpm is available.

Options:
  --help, -h     Show help                                             [boolean]
  --version, -v  Show version number                                   [boolean]
```

## Usage

```js
const caniusePNPM = require('caniuse-pnpm');

if (caniusePNPM()) {
	console.log('pnpm is available. You can use pnpm.');
} else {
	console.log('pnpm is not available! You cannot use pnpm.');
}
```

## Related

- [has-pnpm](https://gitlab.com/gluons/has-pnpm) - Check if a project is using **pnpm**.
- [caniuse-yarn](https://gitlab.com/gluons/caniuse-yarn) - Check if **Yarn** is available.
